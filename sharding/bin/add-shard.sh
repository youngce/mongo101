#!/bin/bash

names=(shardA shardB)
nr=3

for name in "${names[@]}"
do
   echo echo init replica-set for $name
   url="\"$name/"
   
   for (( i=1; i<=$nr; i++ ))
   do
   		id=$((i-1))
		url="${url}sharding_${name}_$i:27017,"
   done
   url=${url%?}'"'
   echo "sh.addShard($url)"
   docker-compose exec --index=1 mongos mongo --eval "sh.addShard($url)"
done
