#!/bin/bash


names=(shardA shardB)
nr=3
for name in "${names[@]}"
do
   echo echo init replica-set for $name
   members='members: [ '
   for (( i=1; i<=$nr; i++ ))
   do
   		id=$((i-1))
		members=${members}'{ _id :'$id' , host : "sharding_'$name'_'$i':27017" },'
   done
   members=${members%?}']'
   docker-compose exec --index=1 $name mongo --eval "rs.initiate( { _id: \"$name\", $members})"

done
