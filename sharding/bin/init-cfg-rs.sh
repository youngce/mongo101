#!/bin/bash
echo init replica-set
docker-compose exec --index=1 config mongo --port 27019 --eval 'rs.initiate( { _id: "configRS", configsvr: true, members: [ { _id : 0, host : "sharding_config_1:27019" }, { _id : 1, host : "sharding_config_2:27019" }, { _id : 2, host : "sharding_config_3:27019" } ] } )'

echo waiting for initialing replica-set 
while :
do
	isMaster=$(docker-compose exec --index=1 config mongo --port 27019 --eval 'rs.isMaster().ismaster'|tail -1|tr -d '\r')

	if $isMaster; then 
		break
	fi
	sleep 5
	
done

echo set chunk size to 1mb
docker-compose exec --index=1 config mongo --port 27019 --eval 'db.settings.save( { _id:"chunksize", value: 1 } )' config
