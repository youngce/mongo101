```bash
docker-compose up \
--scale config=3 \
--scale mongos=2 \
--scale shardA=3 \
--scale shardB=3 -d

```

```bash

## bash
docker-compose exec --index=1 config  mongo --port 27019
```
## mongo shell
```javascript

rs.initiate(
  {
    _id: "configRS",
    configsvr: true,
    members: [
      { _id : 0, host : "sharding_config_1:27019" },
      { _id : 1, host : "sharding_config_2:27019" },
      { _id : 2, host : "sharding_config_3:27019" }
    ]
  }
);
// changing the chunk size to 1 mb(default: 64mb) for demo
db.settings.save( { _id:"chunksize", value: 1 } )
exit
```

```bash
docker-compose exec --index=1 shardA mongo 

rs.initiate(
  {
    _id: "shardA",
    members: [
      { _id : 0, host : "sharding_shardA_1:27017" },
      { _id : 1, host : "sharding_shardA_2:27017" },
      { _id : 2, host : "sharding_shardA_3:27017" }
    ]
  }
)

docker-compose exec --index=1 mongos mongo
sh.addShard("shardA/sharding_shard_1:27017,sharding_shard_2:27017,sharding_shard_3:27017")

sh.enableSharding("test")
sh.shardCollection("test.people", {"age" : 1})

for (i = 0; i < 10*1000; i++) {
  db.people.insert({"name":"person_"+i,age: Math.floor(Math.random() * 100)})
}

sh.shardCollection("test.cpeople", {"name":1,"age" : 1})

for (i = 0; i < 50*1000; i++) {
  db.cpeople.insert({"name":"person_"+i,age: Math.floor(Math.random() * 100)})
}


sh.addShardToZone("shardA", "USPS")
sh.addShardToZone("shardB", "Apple")
// 建立規則
sh.updateZoneKeyRange("test.ips", {"ip" : "056.000.000.000"}, {"ip" : "057.000.000.000"}, "USPS")
sh.updateZoneKeyRange("test.ips", {"ip" : "017.000.000.000"}, {"ip" : "018.000.000.000"}, "Apple")

sh.shardCollection("test.ips", {"ip":1})
function padLeft(str,lenght){
    if(str.length >= lenght)
    return str;
    else
    return padLeft("0" +str,lenght)
}
function randomIp(a,b,c,d){
    
    return [a,b,c,d].map(function(v){
        i= padLeft(Math.floor(Math.random() * 256),3)
        if (v!=null){
            i=padLeft(v,3) 
        } 
        return i
    }).join('.')
}
ips=db.ips
for (i = 0; i < 100*1000; i++) {
    ips.insert({"ip":randomIp(56)})
}
```